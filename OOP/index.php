<?php 

    trait Hewan
    {
        public  $nama,
                $darah = 50,
                $jumlahKaki,
                $keahlian;

        public function __construct( $nama, $jumlahKaki, $keahlian, $attackPower, $defensePower )
        {
            $this->nama = $nama;
            $this->jumlahKaki = $jumlahKaki;
            $this->keahlian = $keahlian;
            $this->attackPower = $attackPower;
            $this->defensePower = $defensePower;
        }

        public function atraksi()
        {
            return "{$this->nama} Sedang {$this->keahlian}";
        }
    }

    trait Fight
    {
        use Hewan;

        public $attackPower, $defensePower;

        public function serang(Hewan $hewan):string{

          return get_class($this) . " sedang menyerang " . get_class($hewan);
      
        }
      
        public function diserang(Hewan $attacker):string{
      
          $sisaDarah = $this->getDarah() - ($attacker->attackPower/$this->getDefencePower());
          $this->setDarah($sisaDarah);
      
          return get_class($this) . " sedang diserang " . get_class($attacker);
        }
      
    }

    class Elang
    {
        use Hewan, Fight;
        private $jenis = "Elang botak";

        public function getElang()
        {
            return $this->nama;
        }

        public function getInfoHewan()
        {
            $str = "Info Hewan : {$this->nama} | Darah : {$this->darah} | Jumlah Kaki : {$this->jumlahKaki} | Keahlian : {$this->keahlian} | Attack Power : {$this->attackPower} | Defense Power : {$this->defensePower} | Jenis : {$this->jenis} |";
            
            return $str ."<br>". $this->atraksi() ."<br>". $this->serang();
        }
    }

    class Harimau
    {
        use Hewan, Fight;
        private $jenis = "Harimau Sumatera";

        public function getHarimau()
        {
            return $this->nama;
        }

        public function getInfoHewan()
        {
            $str = "Info Hewan : {$this->nama} | Darah : {$this->darah} | Jumlah Kaki : {$this->jumlahKaki} | Keahlian : {$this->keahlian} | Attack Power : {$this->attackPower} | Defense Power : {$this->defensePower} | Jenis : {$this->jenis} |";
            
            return $str;
        }
    }

    //Elang
    $hewan1 = new Elang("Elang", 2, "Terbang Tinggi", 10, 5);
    //Harimau
    $hewan2 = new Harimau("Harimau", 4, "Lari Cepat", 7, 8);

    echo $hewan1->getInfoHewan();
    echo "<br>";
    echo "<hr>";
    echo $hewan2->getInfoHewan();
?>